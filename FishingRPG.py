### Import Stuff ###
import os
import time
import sys
import random
import pygame
from pygame.locals import *
from pygame.base import *

### Setup for Main Menu Music ###
VolumeMusic = .5
pygame.mixer.init()
pygame.mixer.music.load('OofFishing.ogg')
pygame.mixer.music.play(-1)
pygame.mixer.music.set_volume(VolumeMusic)

### Game Sound Effects ###
bing = pygame.mixer.Sound('bing.ogg')
def Beep():
    pygame.mixer.Sound.play(bing)

buying = pygame.mixer.Sound('ChaChing.ogg')
def Buy():
    pygame.mixer.Sound.play(buying)

oof = pygame.mixer.Sound('oof.ogg')
def OOF():
    pygame.mixer.Sound.play(oof)
### Create the Player ###
class player:
    def __init__(self):
        self.level = 1
        self.Trout = 0
        self.Salmon = 0
        self.Carp = 0
        self.Money = 0
player = player()

### Unlockable Variables ###
NewSalmon = 0
NewTrout = 0

### Comman Lists/Variables ###
Exhaust = 0
Day = 1
Disable = 0
yes = ["yes", "y", "yee", "yeet"]
no = ["no", "n", "nah", "nada"]

class keyboardDisable():

    def start(self):
        self.on = True

    def stop(self):
        self.on = False

    def __call__(self):
        while self.on:
            msvcrt.getwch()


    def __init__(self):
        self.on = False
        import msvcrt

disable = keyboardDisable()

### Main Menu ###
def MainMenuFE():
    print('''


        OOooOoO ooOoOOo .oOOOo.  o      O            .oOOOo.            `OooOOo.     Oo    Oo      oO    Oo
        o          O    o     o  O      o           .O     o.            o     `o   o  O   O O    o o   o  O
        O          o    O.       o      O           O       o            O      O  O    o  o  o  O  O  O    o
        oOooO      O     `OOoo.  OoOooOOo           o       O            o     .O oOooOoOo O   Oo   O oOooOoOo
        O          o          `O o      O ooooooooo O       o ooooooooo  OOooOO'  o      O O        o o      O
        o          O           o O      o           o       O            o    o   O      o o        O O      o
        o          O    O.    .O o      o           `o     O'            O     O  o      O o        O o      O
        O'      ooOOoOo  `oooO'  o      O            `OoooO'             O      o O.     O O        o O.     O
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                  [1] Play Game
                                                  [2] Options
                                                  [3] Quit Game
    ''')

### Main Menu Logic ###
def MainMenuBE():
    Call = input("> ")
    if Call == "1": #Play Game#
        Beep()
        pygame.mixer.music.stop()
        SetupGame()
    elif Call == "2": #Options#
        os.system('cls')
        OptionsMenu()
    elif Call == "3":
        os.system('cls')
        pygame.mixer.music.stop()
        print("Are You Sure You Want to Exit the Game?")
        Call = input("> ")
        if Call.lower() in yes:
            sys.exit()
        elif Call.lower() in no:
            os.system('cls')
            pygame.mixer.music.play(-1)
            MainMenuFE()
            MainMenuBE()
    else:
        os.system('cls')
        MainMenuFE()
        MainMenuBE()

### Options Menu ###
def OptionsMenu():
    global VolumeMusic
    print('''
###################
##    OPTIONS    ##
###################
  [1]
  [2] Volume
  [3] Back
    ''')
    Call = input("> ")
    if Call == "2":
        print("[U]Up or [D]Down")
        Call = input("> ")
        if Call.lower() == "u":
            VolumeMusic = VolumeMusic + .2
            pygame.mixer.music.set_volume(VolumeMusic)
            os.system('cls')
            OptionsMenu()
        elif Call.lower() == "d":
            VolumeMusic = VolumeMusic - .2
            pygame.mixer.music.set_volume(VolumeMusic)
            os.system('cls')
            OptionsMenu()
    elif Call == "3":
            os.system('cls')
            time.sleep(.1)
            MainMenuFE()
            time.sleep(.1)
            MainMenuBE()
    else:
        print("That is not a Choice. Try Again!")
        MainMenuBE()

### Setup Game (Intro Cutscene) ###
def SetupGame():
    os.system('cls')
    time.sleep(3)
    typing = "Welcome to the Game."
    for character in typing:
        sys.stdout.write(character)
        sys.stdout.flush()
        time.sleep(0.08)
    time.sleep(3)
    os.system('cls')
    GameMenu()

### Initialize Game Menu ###
def GameMenu():
    global NewTrout
    if NewTrout == 1 and Day > 1:
        print("You have unlocked...")
        time.sleep(2)
        Buy()
        print("The FISH SHOP!!!")
        time.sleep(2)
        os.system('cls')
        NewTrout += 1
        GameMenu()
    if Day > 1 and NewTrout == 2:
        print('''
                                         ____    ____
                                        |_   \  /   _|
                                          |   \/   |  .---.  _ .--.  __   _
                                          | |\  /| | / /__\\[ `.-. |[  | | |
                                         _| |_\/_| |_| \__., | | | | | \_/ |,
                                        |_____||_____|'.__.'[___||__]'.__.'_/
                                   ,.  _~-.,               .
                                ~'`_ \/,_. \_
                               / ,"_>@`,__`~.)             |           .
                               | |  @@@@'  ",! .           .          '
                               |/   ^^@     .!  \          |         /
                               `' .^^^     ,'    '         |        .             .
                                .^^^   .          \                /          .
                               .^^^       '  .     \       |      /       . '
                     .,.,.     ^^^             ` .   .,+~'`^`'~+,.     , '
                     &&&&&&,  ,^^^^.  . ._ ..__ _  .'             '. '_ __ ____ __ _ .. .  .
                     %%%%%%%%%^^^^^^%%&&;_,.-=~'`^`'~=-.,__,.-=~'`^`'~=-.,__,.-=~'`^`'~=-.,
                     &&&&&%%%%%%%%%%%%%%%%%%&&;,.-=~'`^`'~=-.,__,.-=~'`^`'~=-.,__,.-=~'`^`'~=
                     %%%%%&&&&&&&&&&&%%%%&&&_,.;^`'~=-.,__,.-=~'`^`'~=-.,__,.-=~'`^`'~=-.,__,
                     %%%%%%%%%&&&&&&&&&-=~'`^`'~=-.,__,.-=~'`^`'~=-.,__,.-==--^'~=-.,__,.-=~'
                     ##mjy#####*"'
                     _,.-=~'`^`'~=-.,__,.-=~'`^`'~=-.,__,.-=~'`^`'~=-.,.-=~'`^`'~=-.,__,.-=~'
        +===============================================================================================+
                                                   [1] Go Fishing
                                                   [2] Fish Shop
                                                   [3] Next Day
                                                   [4] Back
        ''')
        CallG = input("> ")
        if CallG == "1":
            Disable = 1
            GoFishin()
        elif CallG == "2":
            os.system('cls')
            pygame.mixer.music.load('ShopMusic.ogg')
            pygame.mixer.music.play(-1)
            pygame.mixer.music.set_volume(VolumeMusic)
            Shop()
        elif CallG == "3":
            NextDay()
        elif CallG == "4":
            os.system('cls')
            print("Are You Sure You Want to Exit?")
            Ask = input("> ")
            if Ask in yes:
                os.system('cls')
                time.sleep(.1)
                pygame.mixer.music.load('OofFishing.ogg')
                pygame.mixer.music.play(-1)
                pygame.mixer.music.set_volume(VolumeMusic)
                MainMenuFE()
                MainMenuBE()
            else:
                os.system('cls')
                GameMenu()
        else:
            os.system('cls')
            GameMenu()
    elif NewTrout == 0 or Day == 1:
        print('''
                                         ____    ____
                                        |_   \  /   _|
                                          |   \/   |  .---.  _ .--.  __   _
                                          | |\  /| | / /__\\[ `.-. |[  | | |
                                         _| |_\/_| |_| \__., | | | | | \_/ |,
                                        |_____||_____|'.__.'[___||__]'.__.'_/
                                   ,.  _~-.,               .
                                ~'`_ \/,_. \_
                               / ,"_>@`,__`~.)             |           .
                               | |  @@@@'  ",! .           .          '
                               |/   ^^@     .!  \          |         /
                               `' .^^^     ,'    '         |        .             .
                                .^^^   .          \                /          .
                               .^^^       '  .     \       |      /       . '
                     .,.,.     ^^^             ` .   .,+~'`^`'~+,.     , '
                     &&&&&&,  ,^^^^.  . ._ ..__ _  .'             '. '_ __ ____ __ _ .. .  .
                     %%%%%%%%%^^^^^^%%&&;_,.-=~'`^`'~=-.,__,.-=~'`^`'~=-.,__,.-=~'`^`'~=-.,
                     &&&&&%%%%%%%%%%%%%%%%%%&&;,.-=~'`^`'~=-.,__,.-=~'`^`'~=-.,__,.-=~'`^`'~=
                     %%%%%&&&&&&&&&&&%%%%&&&_,.;^`'~=-.,__,.-=~'`^`'~=-.,__,.-=~'`^`'~=-.,__,
                     %%%%%%%%%&&&&&&&&&-=~'`^`'~=-.,__,.-=~'`^`'~=-.,__,.-==--^'~=-.,__,.-=~'
                     ##mjy#####*"'
                     _,.-=~'`^`'~=-.,__,.-=~'`^`'~=-.,__,.-=~'`^`'~=-.,.-=~'`^`'~=-.,__,.-=~'
        +===============================================================================================+
                                                   [1] Go Fishing
                                                   [2] {LOCKED}
                                                   [3] Next Day
                                                   [4] Back
        ''')
        CallG = input("> ")
        if CallG == "1":
            Disable = 1
            GoFishin()
        elif CallG == "2":
            print("This Feature is Currently Locked.")
            time.sleep(3)
            os.system('cls')
            GameMenu()
        elif CallG == "3":
            NextDay()
        elif CallG == "4":
            os.system('cls')
            print("Are You Sure You Want to Exit?")
            Ask = input("> ")
            if Ask in yes:
                os.system('cls')
                time.sleep(.1)
                MainMenuFE()
                MainMenuBE()
            else:
                os.system('cls')
                GameMenu()
        else:
            os.system('cls')
            GameMenu()

### Go Fishing ###
def GoFishin():
    global Exhaust
    global Day
    global NewSalmon
    global NewTrout
    global Disable
    if Exhaust == 0: #Setting Variable for Fishing Once a Day
        pygame.mixer.music.load('GoneFishin.ogg')
        pygame.mixer.music.play(-1)
        pygame.mixer.music.set_volume(VolumeMusic)
        os.system('cls')

        #Amount of Fish per Day:
        Trout = 0
        Salmon = 0

        ### LEVEL 1 ###
        if player.level == 1:
            Energy = 5
            print("You have " + str(Energy) + " Energy Points.")
            print("Spam Enter to Fish!")
            while Energy > 0:
                Casting = input("Cast!")
                Casting = random.randint(1, 101)
                PropOfTrout = list(range(1, 51))
                if Casting in PropOfTrout:
                    if NewTrout == 0:
                        disable.start()
                        pygame.mixer.music.pause()
                        time.sleep(1)
                        print("You Caught a New Fish...")
                        time.sleep(1)
                        Buy()
                        print("You Caught a TROUT!!!")
                        print('''
          /"-._         _
      .--'`    `--.._.-'/
    < o ))     ,       (
      `--._`._(__.--"`.\

                        ''')
                        time.sleep(1)
                        pygame.mixer.music.unpause()
                        NewTrout = 1
                        disable.stop()
                    time.sleep(.1)
                    Trout += 1
                    print("Trout: " + str(Trout))
                    Energy = Energy - 1
                ProbOfNone = list(range(51, 101))
                if Casting in ProbOfNone:
                    MessageRand = random.randint(1,2)
                    time.sleep(.1)
                    if MessageRand == 1:
                        print("You Caught Nothing...")
                    if MessageRand == 2:
                        print("Your Hook Snagged on a Rock...")
                    Energy = Energy - 1
                time.sleep(.4)
            ### What You Get ###
            disable.start()
            player.Trout = player.Trout + Trout
            print("You Caught " + str(Trout) + " Trout!")
            time.sleep(3)
            print("You Have a Total of " + str(player.Trout) + " Trout!")
            time.sleep(3)
            Exhaust = 1 #No Longer Can Fish for that Day
            os.system('cls')
            disable.stop()
            pygame.mixer.music.stop()
            GameMenu()

        ### LEVEL 2 ###
        elif player.level == 2:
            Energy = 7
            print("You have " + str(Energy) + " Energy Points.")
            print("Spam Enter to Fish!")
            while Energy > 0:
                input("Cast!")
                Casting = random.randint(1, 101)
                PropOfTrout = list(range(1,76))
                if Casting in PropOfTrout:
                    Trout += 1
                    print("Trout: " + str(Trout))
                    Energy = Energy - 1
                ProbOfSalmon = list(range(76, 101))
                if Casting in ProbOfSalmon:
                    if NewSalmon == 0:
                        disable.start()
                        pygame.mixer.music.pause()
                        time.sleep(1)
                        print("You Caught a New Fish...")
                        time.sleep(1)
                        Beep()
                        print("You Caught a SALMON!!!")
                        print(r'''
       /`-._
      /_,.._`:-
  ,.-'  ,   `-:..-')
 : o ):';      _  {
  `-._ `'__,.-'\`-.)
      `\\  \,.-'`
                        ''')
                        time.sleep(1)
                        NewSalmon = 1
                        pygame.mixer.music.unpause()
                        disable.stop()
                    Salmon += 1
                    Energy = Energy - 2
                    print("Salmon: " + str(Salmon))
                time.sleep(.4)
            ### What You Caught ###
            if NewSalmon == 1:
                disable.start()
                player.Trout = player.Trout + Trout
                player.Salmon = player.Salmon + Salmon
                print("You Caught " + str(Trout) + " Trout and " + str(Salmon) + " Salmon!")
                time.sleep(3)
                print("You Have a Total of " + str(player.Trout) + " Trout and " + str(player.Salmon) + " Salmon!")
                time.sleep(3)
                Exhaust = 1 #No Longer Can Fish for that Day
                os.system('cls')
                disable.stop()
                pygame.mixer.music.stop()
                GameMenu()
            elif NewSalmon == 0:
                disable.start()
                player.Trout = player.Trout + Trout
                print("You Caught " + str(Trout) + " Trout!")
                time.sleep(3)
                print("You Have a Total of " + str(player.Trout) + " Trout!")
                time.sleep(3)
                Exhaust = 1 #No Longer Can Fish for that Day
                os.system('cls')
                disable.stop()
                GameMenu()

    ### If You Already Fished for the Day ###
    elif Exhaust > 0:
        print("You Have Already Fished for the Day.")
        time.sleep(2)
        os.system('cls')
        GameMenu()

### Shop ~ Where You Buy/Sell Stuff ###
def Shop():
    print(r'''
                           ________  _          __         ______   __
                          |_   __  |(_)        [  |      .' ____ \ [  |
                            | |_ \_|__   .--.   | |--.   | (___ \_| | |--.   .--.   _ .--.
                            |  _|  [  | ( (`\]  | .-. |   _.____`.  | .-. |/ .'`\ \[ '/'`\ \
                           _| |_    | |  `'.'.  | | | |  | \____) | | | | || \__. | | \__/ |
                          |_____|  [___][\__) )[___]|__]  \______.'[___]|__]'.__.'  | ;.__/
                                                                                    [__|
    +==============================================================================================================+
                                         _______________________________
                                        [=U=U=U=U=U=U=U=U=U=U=U=U=U=U=U=]
                                        |.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.|
                                        |        +-+-+-+-+-+-+-+        |
                                        |        |Bait & Tackle|        |
                                        |        +-+-+-+-+-+-+-+        |
                                        |.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.|
                                        |  _________  __ __  _________  |
                                      _ | |___   _  ||[]|[]||  _      | | _
                                     (!)||OPEN|_(!)_|| ,| ,||_(!)_____| |(!)
                                    .T~T|:.....:T~T.:|__|__|:.T~T.:....:|T~T.
                                    ||_||||||||||_|||||||||||||_||||||||||_||
                                    ~\=/~~~~~~~~\=/~~~~~~~~~~~\=/~~~~~~~~\=/~
                                      | -------- | ----------- | -------- |
                                    ~ |~^ ^~~^ ~~| ~^  ~~ ^~^~ |~ ^~^ ~~^ |^~
                              =====================================================
                                                    [1] Buy
                                                    [2] Sell
                                                    [3] Back
    ''')
    Call = input("> ")
    if Call == "1":
        os.system('cls')
        BuyFunc()
    elif Call == "2":
        os.system('cls')
        SellFunc()
    elif Call == "3":
        os.system('cls')
        pygame.mixer.music.stop()
        GameMenu()
    elif Call == "":
        os.system('cls')
        Shop()

### Buying Menu ###
def BuyFunc():
    global buying
    if player.level == 1:
        LevelCost = 200
    if player.level == 2:
        LevelCost = 500
    print(r'''
                                             ______
                                            |_   _ \
                                              | |_) | __   _    _   __
                                              |  __'.[  | | |  [ \ [  ]
                                             _| |__) || \_/ |,  \ '/ /
                                            |_______/ '.__.'_/[\_:  /
                                                               \__.'
    +==============================================================================================================+
                                         E              E              E
                                      .x+E:..        .x+E:..        .x+E:..
                                    u8~  E  `b.    u8~  E  `b.    u8~  E  `b.
                                   t8E   E d888>  t8E   E d888>  t8E   E d888>
                                   88N.  E'8888~  88N.  E'8888~  88N.  E'8888~
                                   88888b&.`""`   88888b&.`""`   88888b&.`""`
                                   '88888888e.    '88888888e.    '88888888e.
                                     "*8888888N     "*8888888N     "*8888888N
                                    uu. ^8*8888E   uu. ^8*8888E   uu. ^8*8888E
                                   @888L E `"88E  @888L E `"88E  @888L E `"88E
                                  '8888~ E   98~ '8888~ E   98~ '8888~ E   98~
                                   `*.   E  .*"   `*.   E  .*"   `*.   E  .*"
                                     `~==R=~`       `~==R=~`       `~==R=~`
                              =====================================================
                                                [1] Level Up-$''' + LevelCost + '''
                                                [2] Back
    ''')
    Call = input("> ")
    if Call == "yeet":
        os.system('cls')
        print("Are You Sure You Want to Buy the {}?".format("ZooF"))
        Call = input("> ")
        if Call in yes:
            OOF()
            print("That's a {}.... rip yr mula.".format("ZooF"))
            player.Money = 0
            print("Your Really Dumb.")
            time.sleep(4.20)
            os.system('cls')
            Shop()
        elif Call in no:
            OOF()
            print("You can't avoid the {}.... rip yr mula.".format("ZooF"))
            player.Money = 0
            print("Your Really Dumb.")
            time.sleep(4.20)
            os.system('cls')
            Shop()
        else:
            os.system('cls')
            BuyFunc()
    elif Call == "1":
        os.system('cls')
        print("Are You Ready to Level Up?")
        Call = input("> ")
        if Call in yes:
            if player.Money >= LevelCost:
                Buy()
                player.Money = player.Money - LevelCost
                player.level += 1
                print("You are now Level " + str(player.level) + "!")
                time.sleep(3)
                os.system('cls')
                Shop()
            elif player.Money < LevelCost:
                OOF()
                print("You Don't Have Enough Money...")
                time.sleep(3)
                os.system('cls')
                BuyFunc()
        else:
            os.system('cls')
            BuyFunc()
    elif Call == "2":
        os.system('cls')
        Shop()
    else:
        os.system('cls')
        BuyFunc()

### Selling Menu ###
def SellFunc():
    global buying
    global NewSalmon
    print( """
                                            ______          __   __
                                          .' ____ \        [  | [  |
                                          | (___ \_| .---.  | |  | |
                                           _.____`. / /__\\ | |  | |
                                          | \____) || \__., | |  | |
                                           \______.' '.__.'[___][___]
    +==============================================================================================================+
                                     o
                                    o      ______/~/~/~/__           /((
                                      o  // __            ====__    /_((
                                     o  //  @))       ))))      ===/__((
                                        ))           )))))))        __((
                                        \\     \)     ))))    __===\ _((
                                         \\_______________====      \_((
                                                                     \((
                              =====================================================
                                              [1] Trout- $10/fish""")
    if NewSalmon == 1:
        print("                                              [2] Salmon- $25/fish")
        print("                                              [3] Back")
        Call = input("> ")
        if Call == "1":
            os.system('cls')
            print("You Have {} Trout.".format(player.Trout))
            print("How Many Do You Want to Sell?")
            Call = int(input("> "))
            try:
                if player.Trout >= Call:
                    if Call == 0:
                        os.system('cls')
                        SellFunc()
                    Buy()
                    player.Trout = player.Trout - Call
                    MoneyRecived = Call * 10
                    player.Money = player.Money + MoneyRecived
                    print("You Recived $" + str(MoneyRecived))
                    print("Total Money: $" + str(player.Money))
                    time.sleep(3)
                    os.system('cls')
                    SellFunc()
                elif player.Trout < Call:
                    OOF()
                    print("You Can't Sell More Fish Then You Have!!!")
                    time.sleep(3)
                    os.system('cls')
                    SellFunc()

            except:
                os.system('cls')
                SellFunc()
        elif Call == "2":
            os.system('cls')
            print("You Have {} Salmon.".format(player.Salmon))
            print("How Many Do You Want to Sell?")
            Call = int(input("> "))
            try:
                if player.Salmon >= Call:
                    if Call == 0:
                        os.system('cls')
                        SellFunc()
                    Buy()
                    player.Salmon = player.Salmon - Call
                    MoneyRecived = Call * 25
                    player.Money = player.Money + MoneyRecived
                    print("You Recived $" + str(MoneyRecived))
                    print("Total Money: $" + str(player.Money))
                elif player.Salmon < Call:
                    OOF()
                    print("You Can't Sell More Fish Then You Have!!! oooof...")
                time.sleep(3)
                os.system('cls')
                SellFunc()
            except:
                SellFunc()
        elif Call == "3":
            os.system('cls')
            Shop()
        else:
            os.system('cls')
            SellFunc()
    if NewSalmon == 0:
        print("                                              [2] Back")
        Call = input("> ")
        if Call == "1":
            os.system('cls')
            print("You Have {} Trout.".format(player.Trout))
            print("How Many Do You Want to Sell?")
            Call = int(input("> "))
            try:
                if player.Trout >= Call:
                    if Call == 0:
                        os.system('cls')
                        SellFunc()
                    player.Trout = player.Trout - Call
                    MoneyRecived = Call * 10
                    player.Money = player.Money + MoneyRecived
                    Buy()
                    print("You Recived $" + str(MoneyRecived))
                    print("Total Money: $" + str(player.Money))
                elif player.Trout < Call:
                    OOF()
                    print("You Can't Sell More Fish Then You Have!!! ooph...")
                time.sleep(3)
                os.system('cls')
                SellFunc()
            except:
                os.system('cls')
                SellFunc()
        elif Call == "2":
            os.system('cls')
            Shop()
        else:
            os.system('cls')
            SellFunc()

### Next Day ("Cutscene") ##
def NextDay():
    global Exhaust
    global Day
    os.system('cls')
    print("End of Day " + str(Day))
    time.sleep(2)
    Day += 1
    print("Beginning of Day " + str(Day))
    time.sleep(3)
    os.system('cls')
    Exhaust = 0
    GameMenu()

os.system('cls')
MainMenuFE()
MainMenuBE()
